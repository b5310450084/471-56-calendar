package Calendar;

import java.util.Date;

public interface TimeDuration {
	public String getDescription();
	public Date getStartTime();
	public int getDuration();
}
