package Calendar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Task extends AbstractTimeDuration {
	
	
	Date startTime,endTime;
	Boolean done;
	int estTime,totalEstTime=0;
	DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	
	protected  Task(String description,String startTime, String estTime) throws ParseException {
		this.description = description;
	
		this.startTime = dateTimeFormat.parse(startTime);
		this.estTime =  Integer.parseInt(estTime);
		
		// TODO Auto-generated constructor stub
	}
	
	

	protected boolean isDone(){
		 done = true;
		 return done;
	}
	protected Boolean isNotDone(){
		done = false;
		 return done;
	}
	/**
	 * ���¡�� ��������� Task
	 * @return
	 */
	public Date getStartTime(){
		
		
		return startTime;
	}
	
	public int getDuration(){
		
		return estTime ;
	}
	
public String toString(){
		
		return "  "+description+"\tTask\t        "+dateFormat.format(startTime) +"\t"+"\t  "+ getDuration()+"\t\t"+done+"\n";
		
	}
}
