package Calendar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Event extends AbstractTimeDuration {
	
	Date from,end,date;
	int estTime;
	int totalEstTime=0;
	DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	
	protected  Event(String description,String from, String end) throws ParseException {
		this.description = description;
		this.from = dateTimeFormat.parse(from);
		this.end = dateTimeFormat.parse(end);
		
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	/**
	 * ���¡������������� Event
	 */
	public Date getStartTime(){
		
		
		return from;
	}
	/**
	 * ���¡�� �������� Event
	 * @return
	 */
	public Date endTime(){
		
		
		return end;
	}
	/**
	 * �ӹǹ�������ҷ������ͧ Event
	 */
	public int getDuration(){
		estTime  =  (int)((end.getTime() - from.getTime())/(60*1000));
		
		return estTime ;
		
	}
	
	
	public String toString(){
		return "  "+description+"\tEvent\t        "+dateFormat.format(from)+"\t"+"\t  "+ getDuration()+"\t\t-\n";
		
	}
}
