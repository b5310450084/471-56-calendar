package Calendar;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class EventCase {
	
	

	@Test
	public void testGetDuration() throws ParseException {
		Event e = new Event( "test","07/01/13 14:00","07/01/13 15:00");
		int result = e.getDuration();
		assertEquals("",60, result);
	}

}
