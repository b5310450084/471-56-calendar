package Calendar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;



public class Console  {
	int i,j;
	String action,data;
	String[] info;
	TimeDuration event;
	Account a = new Account();
	ReadWriteFile rw = new ReadWriteFile();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	ConsoleManager console = ConsoleManager.getInstance();
	
	
	public Console() throws IOException, ParseException{
		
		System.out.println("Welcome to My Calendar");
		
	}
	
	public void startCalendar() throws IOException, ParseException{
		int x = 0;
		do
		{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nChoose action(c= Create d = Displays l=LoadData s=saveData q=quit) : ");
		String action = br.readLine().trim();
		action(action);
		} while ( x==0 );
	}
	
	public void action(String action) throws ParseException, IOException{
		switch ( action )
		{
		     case "c" : console.create();
						System.out.println("done adding");
		                break;
		     case "d" : console.display();
		    	 			
				
						break;
		     case "s" : console.saveData();
		     			System.out.println("File Saved");
				
						break;
		     case "l" : console.loadData();
		     			System.out.println("File Loaded");
				
						break;
		     case "q" :
		    	
		     	
		     	System.exit(0);
				
                break;
		}
		

	}
}
