package Calendar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Observable;

public class ConsoleManager  extends Observable  {

	int i,j;
	String action,data;
	String[] info;
	TimeDuration td,event;
	Account a = new Account();
	ReadWriteFile rw = new ReadWriteFile();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	public String x = "    Item    \tType\t          Date \t\tEst.Time   \t\tDone\n#############################################################################################\n";
	
	private static ConsoleManager instance = null;
	 
    private ConsoleManager() {
    	
    }
    
    public static ConsoleManager getInstance() {
        if (instance == null) {
            instance = new ConsoleManager();
        }
 
        return instance;
    }
    

	/**
	 * �����ҧEvent�ѹTask ��ҹconsole
	 * @throws ParseException
	 * @throws IOException
	 */
	public void create() throws ParseException, IOException{
		System.out.println("Create task or event(t = task e = event) : ");
		String action = br.readLine().trim();
		
		switch ( action )
		{
		case "e" :	System.out.println("Add event(desc,form,to) : ");
					data = br.readLine().trim();
					info = data.split(",");
					event = new Event(info[0].trim(),info[1].trim(),info[2].trim());
					data = ("Event,"+data);
					tmpData(data);
					a.addEvent((TimeDuration) event);
					break;
		case "t" :	System.out.println("Add event(desc,due date,time duration) : ");
					data = br.readLine().trim();
					info = data.split(",");
					event = new Task(info[0].trim(),info[1].trim(),info[2].trim());
					data = ("Task,"+data);
					tmpData(data);
					a.addEvent((TimeDuration) event);
					break;
		}
	
		
	}
	/**
	 * �����ҧEvent �ѹ Task ��ҹ�ҧ GUI
	 * @param event
	 */
	public void createGUI(TimeDuration event){
		a.addEvent(event);
		setChanged();
		notifyObservers(event);
		
	}
	/**
	 * �ʴ������� Event ��� Task ��������ҧ��� ��ҹ�ҧ console
	 */
	public void display(){
	 sortByDate();
	
   	 System.out.println("   Item    \tType\t\t  Date \t\t   Est.Time   \t\tDone");
   	 System.out.println("###########\t####\t\t######### \t   #########\t\t####");
   	j = a.event.size();
   		for(i=0;i<j;i++){				
	System.out.println(a.event.get(i));
	}
   	 System.out.println("\tTotal Est.Time		"+a.getTotalEstTime()+"	Minutes");
		
	}
	

	
	
	public int getTotalTime(){
		int total = 0;
		for(int i=0;i<a.event.size();i++){
			td = a.event.get(i);
			total = total + td.getDuration();		
		}
		
		return total;
	}
	/**
	 * ��Ŵ�����ŷ��ѹ�֡���
	 * @throws IOException
	 * @throws ParseException
	 */
	public void loadData() throws IOException, ParseException{
		rw.ReadFile();
		a = rw.a;
	}
	/**
	 * ૿������
	 * @throws IOException
	 */
	public void saveData() throws IOException{
		rw.WriteFile();
	}
	/**
	 * ���ͧ�����ŷ�������ҧ event �ѹ Task ���
	 * @param data
	 * @throws IOException
	 */
	public void tmpData(String data) throws IOException{
	
		rw.TempFile(data);
	}
	/**
	 * ���§�����ŵ���ѹ���
	 */
	
	
	
	public void sortByDate(){
		Collections.sort(a.event, new Comparator<TimeDuration>() {

			
			@SuppressWarnings("deprecation")
			@Override
			public int compare(TimeDuration o1, TimeDuration o2) {
				if (o1.getStartTime().getDate() < o2.getStartTime().getDate())
				return -1;
				if (o1.getStartTime().getDate() > o2.getStartTime().getDate())
				return 1;
				
				return 0;
				}
				});
		
		
		}
	/**
	 * ���¢����ŵ������ѡ��
	 */
	public void sortByDesc(){
		Collections.sort(a.event, new Comparator<TimeDuration>() {

			@Override
			public int compare(TimeDuration o1, TimeDuration o2) {
				
				return o1.getDescription().compareTo(o2.getDescription());
			}


		
		});
	}
	/**
	 * ���§�����ŵ���������ҡ�÷ӧҹ
	 */
	public void sortByDuration(){
		Collections.sort(a.event,new Comparator<TimeDuration>(){

			@Override
			public int compare(TimeDuration o1, TimeDuration o2) {
				if (o1.getDuration() < o2.getDuration())
				return -1;
				if  (o1.getDuration() > o2.getDuration())
				return 1;
				
				return 0;
				}
				});
	

	}
}