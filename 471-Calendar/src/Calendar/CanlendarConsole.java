package Calendar;

import java.awt.EventQueue;
import java.io.IOException;
import java.text.ParseException;



public class CanlendarConsole {
	
	/**
	 * @param args
	 * @throws ParseException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws ParseException, IOException {
	
		Console console = new Console();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					GuiConsole frame = new GuiConsole();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		console.startCalendar();
		
		
				
	}	
	
}
	
