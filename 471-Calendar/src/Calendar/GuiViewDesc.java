package Calendar;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextArea;
import java.io.IOException;
import java.text.ParseException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JTextArea;
import javax.swing.DropMode;

public class GuiViewDesc extends JFrame implements Observer {

	private JPanel contentPane;
	private JTextArea textArea;
	protected GuiConsole view;
	ConsoleManager console = ConsoleManager.getInstance();
	//protected String a;


	/**
	 * Create the frame.
	 * @param a 
	 * @param string 
	 * @throws ParseException 
	 * @throws IOException 
	 */
	public GuiViewDesc() throws IOException, ParseException {
		create();
		setDefault();
	}
	public void create(){

		setVisible(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 680, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "SortByDescription", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 649, 440);
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		
		 textArea = new JTextArea();
		textArea.setEnabled(true);
		textArea.setBounds(10, 24, 629, 405);
		panel.add(textArea);
		
		
		
		
		
		//textArea.setText(console.x+console.a.List());
		//textArea.setText(a);
	}
	protected void setDefault() throws IOException, ParseException {
		
		console.sortByDesc();
		textArea.setText(console.x+console.a.List());
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		console.sortByDesc();
		textArea.setText(console.x+console.a.List());
		// TODO Auto-generated method stub
		
	}
}