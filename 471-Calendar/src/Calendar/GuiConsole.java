package Calendar;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextArea;

import javax.naming.ldap.Rdn;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import java.awt.FlowLayout;
import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JSpinner;
import java.awt.List;
import javax.swing.JToolBar;
import javax.swing.JList;
import java.awt.Choice;
import java.text.ParseException;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Label;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JTextArea;



public class GuiConsole extends JFrame implements Observer {
	
	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private String data;
	private boolean du,date,desc = false;
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	GuiViewDesc viewDesc = new GuiViewDesc();
	GuiViewDate viewDate = new GuiViewDate();
	GuiViewDuration viewDu = new GuiViewDuration();
	TimeDuration event;
	ConsoleManager console = ConsoleManager.getInstance();

	/**
	 * Create the frame.
	 * @throws ParseException 
	 * @throws IOException 
	 */
	public  GuiConsole() throws ParseException, IOException{
		create();
		
		
		console.addObserver(viewDesc);
		console.addObserver(viewDate);
		console.addObserver(viewDu);
	}
	public void create() throws ParseException, IOException {
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 200, 365, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 328, 178);
		panel.setBorder(new TitledBorder(null, "Create", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 200, 328, 106);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Display Sort By", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblType.setBounds(92, 23, 46, 21);
		panel.add(lblType);
		
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblDescription.setBounds(61, 55, 77, 21);
		panel.add(lblDescription);
		
		JLabel lblFrom = new JLabel("From");
		lblFrom.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblFrom.setBounds(92, 86, 46, 14);
		panel.add(lblFrom);
		
		final JLabel lblTo = new JLabel("To");
		lblTo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTo.setBounds(102, 111, 46, 14);
		panel.add(lblTo);
		
		final JLabel lblResult = new JLabel("");
		lblResult.setBounds(207, 140, 111, 19);
		panel.add(lblResult);
		
		
		textField_1 = new JTextField();
		textField_1.setBounds(148, 56, 170, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(148, 84, 170, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(148, 109, 170, 20);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		final JLabel lblDuration = new JLabel("Duration");
		lblDuration.setVisible(false);
		lblDuration.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblDuration.setBounds(76, 109, 48, 18);
		panel.add(lblDuration);
		
		final JRadioButton rdbtnEvent = new JRadioButton("Event");
		rdbtnEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTo.setVisible(true);
				lblDuration.setVisible(false);
			}
		});
		buttonGroup.add(rdbtnEvent);
		rdbtnEvent.setBounds(147, 23, 77, 23);
		panel.add(rdbtnEvent);
		
		final JRadioButton rdbtnTask = new JRadioButton("Task");
		rdbtnTask.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTo.setVisible(false);
				lblDuration.setVisible(true);
				
			}
		});
		buttonGroup.add(rdbtnTask);
		rdbtnTask.setBounds(226, 23, 85, 23);
		panel.add(rdbtnTask);
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtnEvent.isSelected()){
					try {
						 event = new Event(textField_1.getText(), textField_2.getText(), textField_3.getText());
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					data = ("Task,"+data);
					try {
						console.tmpData(data);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					console.createGUI(event);
					lblResult.setText("Adding done");
				}
				if(rdbtnTask.isSelected()){
					try {
						 event = new Task(textField_1.getText(), textField_2.getText(), textField_3.getText());
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					data = ("Task,"+data);
					try {
						console.tmpData(data);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					console.createGUI(event);
					lblResult.setText("Adding done");
			
				}
				textField_1.setText(null);
				textField_2.setText(null);
				textField_3.setText(null);
			}
		});
		btnAdd.setBounds(112, 144, 85, 23);
		panel.add(btnAdd);
		
	
		
		
		
		
		JButton SortBotton = new JButton("Date");
		SortBotton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(date == false){
					date = true;
				viewDate.setVisible(date);
				}
				else if(date == true){
					date = false;
					viewDate.setVisible(date);	
				}
				
			}
		});
		SortBotton.setBounds(21, 46, 89, 23);
		panel_1.add(SortBotton);
		
		JButton btnDescription = new JButton("Description");
		btnDescription.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(desc == false){
					desc = true;
				viewDesc.setVisible(desc);
				}
				else if(desc == true){
					desc = false;
					viewDesc.setVisible(desc);	
				}
				
		
			}
		});
		btnDescription.setBounds(120, 46, 89, 23);
		panel_1.add(btnDescription);
		
		JButton btnDuration = new JButton("Duration");
		btnDuration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(du == false){
					du = true;
				viewDu.setVisible(du);
				}
				else if(du == true){
					du = false;
					viewDu.setVisible(du);	
				}
				
			}
		});
		btnDuration.setBounds(219, 46, 89, 23);
		panel_1.add(btnDuration);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Save&Load File", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 317, 328, 51);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JButton btnLoadfile = new JButton("LoadFile");
		btnLoadfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					console.rw.ReadFile();
					console.a = console.rw.a;
					viewDu.setDefault();
					viewDate.setDefault();
					viewDesc.setDefault();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				lblResult.setText("Load success");
				
			}
		});
		btnLoadfile.setBounds(179, 17, 89, 23);
		panel_3.add(btnLoadfile);
		
		JButton btnSavefile = new JButton("SaveFile");
		btnSavefile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					console.rw.WriteFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lblResult.setText("Saved success");
			}
		});
		btnSavefile.setBounds(56, 17, 89, 23);
		panel_3.add(btnSavefile);
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
	}
